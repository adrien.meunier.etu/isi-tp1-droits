#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <check_pass.h>

#define TAILLE_MAX 1000
#define FILE_PASSWORD "./home/admin/passwd"

int main(int argc, char *argv[])
{
    FILE *f;
    if (argc < 2) {
        printf("Missing argument \n");
        exit(EXIT_FAILURE);
    }

    int uid_user = (int) getuid();
    int gid_user = (int) getuid();

    struct stat fileStat;

    // File not accessible
    if(stat(argv[1], &fileStat) < 0)    
        exit(EXIT_FAILURE);

    // Not the same group
    if (fileStat.st_gid != gid_user) 
        exit(EXIT_FAILURE);

    // No Permission to remove
    if (!(fileStat.st_mode & S_IWGRP) & !(fileStat.st_mode & S_IWOTH))
        exit(EXIT_FAILURE);

    // Openning the file
    f = fopen(FILE_PASSWORD, "r");
    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    int found = 1;
    char chaine[TAILLE_MAX] = "";
    // UID est codé sur 15 bits
    char uid[16];
    sprintf(uid, "%d", uid_user);
    int length_uid = strlen(uid);
    

    while (fgets(chaine,TAILLE_MAX,f) != NULL) {
        for (int i=0;i<length_uid;i++) {
            if (chaine[i] != uid[i]);
                break;
        }
        if (check_pass(length_uid+1,chaine)) {
            fclose(f);
            remove(argv[1]);
            exit(EXIT_SUCCESS);
        }
    }



    fclose(f);

    exit(EXIT_FAILURE);
}
