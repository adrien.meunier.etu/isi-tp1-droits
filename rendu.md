# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Meunier, Adrien, M1-ML , email: adrien.meunier.etu@univ-lille.fr

- Nom, Prénom, email: ___

## Question 1

Création de l'utilisateur toto du groupe ubuntu :  sudo adduser toto ubuntu  

-r--rw-r-- 1 toto ubuntu 5 Jan 5 09:35 titi.txt

La permissions d'écriture est seulement donné au groupe toto (troisième colonne).  

Cependant :  
~$ id toto
uid=1001(toto) gid=1000(ubuntu) groups=1000(ubuntu)

Dans notre cas, toto n'appartient seulement au groupe ubuntu. L'appartenance d'un utilisateur a un groupe portant son nom n'est pas induit comme le cas de l'utilisateur ubuntu peut nous le montrer :
~$ id ubuntu
uid=1000(ubuntu) gid=1000(ubuntu) groups=1000(ubuntu),4(adm),20(dialout),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),117(netdev),118(lxd)

Donc le processus lancé par toto ne peut pas écrire dans ce fichier.
  
## Question 2

Le caractère x pour répertoire signifie si le dossier est accéssible.

drwxrw-r-x 2 ubuntu ubuntu 4096 Jan 13 16:23 mydir
toto@vm1:/home/ubuntu$ cd mydir
bash: cd: mydir: Permission denied

toto@vm1:/home/ubuntu$ ls -al mydir
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt


## Question 3

Ubuntu:
UID : 1000 
GID : 1000 
EUID : 1000 
EGID : 1000 
File opens correctly 

toto:
UID : 1001 
GID : 1000 
EUID : 1001 
EGID : 1000 
Cannot open file: Permission denied

Plus x mais s
-rwsrwxr-x 1 ubuntu ubuntu 17088 Jan 13 17:08 a.out

toto@vm1:/home/ubuntu$ ./a.out mydir/data.txt
UID : 1001 
GID : 1000 
EUID : 1000 
EGID : 1000 
File opens correctly

ubuntu@vm1:~$ python3 perm.py
EUID : 1000
EGID : 1000

-rwsrw-r-- 1 ubuntu ubuntu    69 Jan 13 17:21 perm.py
toto@vm1:/home/ubuntu$ python3 perm.py
EUID : 1001
EGID : 1000


## Question 4


## Question 5

chfn - Modifier le nom complet et les informations associées à un utilisateur
Tous les utilisateurs peuvent ouvrir et exécuter la commande mais seul l'adiministrateur (root) peut le modifier.
Les autres utilisateurs n'ont pas la permission d'utiliser les options de chfn

ubuntu@vm1:~$ chfn toto

toto:x:1001:1000:,12,0627389789,:/home/toto:/bin/bash

## Question 6

Les mots de passes sont stocké dans /etc/shadow

buntu@vm1:~$ ls -al /etc/shadow
-rw-r----- 1 root shadow 1144 Jan 13 15:48 /etc/shadow

Les mots de passes sont stocké dans un autre fichier pour éviter que des informations soit récupérer en même temps qu'une lecture du fichier password qui est lisible. Alors que shadow est seulement lisible par l'administrateur et tout utilisateur membre de son groupe.

Les mots de passes sont encrypté dans le fichier.

## Question 7

Les fichiers dir_a auront rwxtr_x___ lambda_a group_a
Le même principe sera appliqué pour les fichiers dir_b
Les fichiers dir_c auront rwxt___r_x.
Cependant dans cette situation, admin ne pourra pas supprimer les fichier dans dir_a et dir_b.
## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








